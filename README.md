## API Endpoint Testing Using Python

I completed the technical assessment to create automation script for testing the API endpoint. To run the test dowland the python file "api_test.py" and follow the steps,

## Steps to Run the test

1. To run the test download and install python https://www.python.org/downloads/

![picture](readmeImage/tempsnip.png)

2. Open Python IDE

![picture](readmeImage/openPython.png)

3. Open python test file "api_test.py" (file -> open -> file location)

![picture](readmeImage/openFile.png)

4. Run the test (Run -> Run Module)

![picture](readmeImage/runFile.png)

