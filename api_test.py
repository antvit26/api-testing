#!/usr/bin/env python3
import json
import unittest
import urllib.request


API_URL = "https://api.tmsandbox.co.nz/v1/Categories/6328/Details.json?catalogue=false"


class TestApiCall(unittest.TestCase):
    """test class to verify api response
    """
    def setUp(self):
        """sending api request and parse response from json to dict
        """
        request_obj = urllib.request.Request(API_URL)
        self.response = urllib.request.urlopen(request_obj)
        self.response_data = json.loads(self.response.read())

    def test_name(self):
        """checking name is 'Badges'
        """
        self.assertEqual(self.response_data['Name'], 'Badges')

    def test_can_list_class_fields(self):
        """cheking CanListClassifieds value is false
        """
        self.assertFalse(self.response_data['CanListClassifieds'])

    def test_charities_plunket_tagline(self):
        """Charities element with Description "Plunket" has a Tagline 
        that contains the text "well child health services"
        """
        charities = self.response_data['Charities']

        for charity in charities:
            if charity['Description'] == 'Plunket':
                self.assertIn('well child health services', charity['Tagline'])

    def tearDown(self):
        """closing response instance
        """
        self.response.close()


if __name__ == '__main__':
    unittest.main()
